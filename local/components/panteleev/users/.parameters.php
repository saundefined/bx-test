<?php

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

try {
    Loader::includeModule('iblock');
} catch (LoaderException $e) {
}

Loc::loadMessages(__FILE__);

$arComponentParameters = [
    'GROUPS' => [
        'GENERAL' => [
            'NAME' => Loc::getMessage('PANTELEEV_USERS_SETTINGS_GENERAL')
        ],
    ],
    'PARAMETERS' => [
        'PAGE_SIZE' => [
            'PARENT' => 'GENERAL',
            'NAME' => Loc::getMessage('PANTELEEV_USERS_SETTINGS_PAGE_SIZE'),
            'TYPE' => 'STRING',
            'DEFAULT' => '5',
        ],
        'AJAX_MODE' => []
    ]
];