<?php

use Bitrix\Main\Context;
use Panteleev\Components\Users;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

\CBitrixComponent::includeComponentClass('panteleev:users');

$query = Context::getCurrent()->getRequest()->getQueryList()->toArray();

$component = new Users();
$component->exportData($query);