<?php

namespace Panteleev\Components;

use Bitrix\Main\UI\Extension;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\UserTable;
use CBitrixComponent;


class Users extends CBitrixComponent
{
    private $nav;

    private $parameters;

    private $gridId = 'panteleev_users_list';

    public function exportData($post)
    {
        $users = [];
        $query = UserTable::getList([
            'filter' => ['ID' => explode(',', $post['ids'])],
            'select' => ['ID', 'NAME', 'LOGIN'],
        ]);
        while ($ar = $query->fetch()) {
            $users[] = $ar;
        }

        switch ($post['format']) {
            case 'xml':
                $this->exportDataXML($users);
                break;
            case 'csv':
                $this->exportDataCSV($users);
                break;
        }

    }

    private function exportDataXML($users = [])
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
        $xml .= '<users>';
        foreach ($users as $item) {
            $xml .= '<user>';
            $xml .= '<id>' . $item['ID'] . '</id>';
            $xml .= '<name>' . $item['NAME'] . '</name>';
            $xml .= '<login>' . $item['LOGIN'] . '</login>';
            $xml .= '</user>';
        }
        $xml .= '</users>';

        header('Content-Type: text/xml');
        header('Content-Disposition: attachment; filename="users.xml"');

        echo $xml;
    }

    private function exportDataCSV($users = [])
    {
        $csv = 'ID;Имя;Логин' . PHP_EOL;
        foreach ($users as $item) {
            $csv .= implode(';', $item) . PHP_EOL;
        }

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="users.csv"');

        echo $csv;
    }

    public function executeComponent()
    {
        $this->prepareComponent();
        $this->getParameters();
        $this->initNavigation();

        $this->arResult['GRID_ID'] = $this->gridId;

        $this->arResult['USERS'] = $this->getUsers();

        $this->arResult['NAV_OBJECT'] = $this->getNav();

        $this->includeComponentTemplate();
    }

    private function prepareComponent()
    {
        Extension::load(['ui.bootstrap4', 'jquery2']);
    }

    private function getParameters()
    {
        $defaultParameters = [
            'PAGE_SIZE' => '5',
            '~PAGE_SIZE' => '5',
        ];

        $this->parameters = array_merge($defaultParameters, $this->arParams);
    }

    private function initNavigation()
    {
        $this->nav = new PageNavigation($this->gridId);
        $this->nav->allowAllRecords(true)
            ->setPageSize($this->parameters['PAGE_SIZE'])
            ->initFromUri();
    }

    private function getUsers()
    {
        $query = UserTable::getList([
            'filter' => [],
            'select' => ['ID', 'NAME', 'LOGIN'],
            'count_total' => true,
            'offset' => $this->nav->getOffset(),
            'limit' => $this->nav->getLimit(),
        ]);

        $this->nav->setRecordCount($query->getCount());

        return $query->fetchAll();
    }

    private function getNav()
    {
        return $this->nav;
    }
}