<?php

$MESS['PANTELEEV_USERS_COMPONENT_NAME'] = 'Пользователи';
$MESS['PANTELEEV_USERS_COMPONENT_DESCRIPTION'] = 'Компонент с применением технологий ядра D7, который выводит список зарегистрированных пользователей с постраничной навигацией. Постраничная навигация должна осуществляться Ajax-ом. Сделать возможность выгрузки пользователей в csv и xml формат.';
$MESS['PANTELEEV_USERS_COMPONENT_GROUP_NAME'] = 'Тестовые компоненты';
