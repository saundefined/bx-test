$(document).ready(function() {
  $(document).on('click', '[data-export-button]', function(e) {
    e.preventDefault();

    var ids = getExportedElements($(this).parents('[data-export-form]')),
        format = $(this).attr('data-export-button');

    window.location.href = panteleev_users_path + '?format=' + format +
        '&ids=' + ids.join(',');
  });

  $(document).on('change', '[data-export-form]', function(e) {
    e.preventDefault();

    var ids = getExportedElements($(this));

    $('[data-export-button]').attr('disabled', ids.length <= 0);
  });
});

function getExportedElements(node) {
  var ids = [],
      checkbox = node.find('input[type=checkbox][name=\'ids[]\']:checked');

  $.each(checkbox, function() {
    ids.push($(this).val());
  });

  return ids;
}