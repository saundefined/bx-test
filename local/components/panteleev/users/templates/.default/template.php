<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<? if (!empty($arResult['USERS'])) { ?>
    <div class="users">
        <form action="#" method="get" data-export-form>
            <div class="panel">
                <button data-export-button="xml" class="btn btn-primary" disabled>Экспортировать XML</button>
                <button data-export-button="csv" class="btn btn-primary" disabled>Экспортировать CSV</button>
            </div>
            <table class="table">
                <tr>
                    <td></td>
                    <td>ID</td>
                    <td>Логин</td>
                    <td>Имя</td>
                </tr>
                <? foreach ($arResult['USERS'] as $item) { ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="ids[]" value="<?= $item['ID']; ?>"/>
                        </td>
                        <td><?= $item['ID']; ?></td>
                        <td><?= $item['LOGIN']; ?></td>
                        <td><?= $item['NAME'] ? '(' . $item['NAME'] . ')' : ''; ?></td>
                    </tr>
                <? } ?>
            </table>
        </form>
    </div>
    <? $APPLICATION->IncludeComponent(
        'bitrix:main.pagenavigation',
        '',
        [
            'NAV_OBJECT' => $arResult['NAV_OBJECT'],
        ],
        false,
        ['HIDE_ICONS' => 'Y']
    ); ?>
<? } else { ?>
    <? ShowMessage(['TYPE' => 'ERROR', 'MESSA   GE' => 'Пользователи не найдены']); ?>
<? } ?>

<script>
  var panteleev_users_path = '<?=$componentPath;?>/ajax.php';
</script>
