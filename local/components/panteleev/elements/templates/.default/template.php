<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

?>

<? if (!empty($arResult['ERRORS'])) { ?>
    <? ShowMessage(['TYPE' => 'ERROR', 'MESSAGE' => implode('<br />', $arResult['ERRORS'])]); ?>
<? } ?>

<? if (!empty($arResult['ELEMENTS'])) { ?>
    <div class="elements">
        <ul class="elements__list">
            <? foreach ($arResult['ELEMENTS'] as $item) {
                $this->AddEditAction($item['ID'], $item['EDIT_LINK'],
                    CIBlock::GetArrayByID($item['IBLOCK_ID'], 'ELEMENT_EDIT'));
                $this->AddDeleteAction($item['ID'], $item['DELETE_LINK'],
                    CIBlock::GetArrayByID($item['IBLOCK_ID'], 'ELEMENT_DELETE'),
                    ['CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')]);
                ?>
                <li id="<?= $this->GetEditAreaId($item['ID']); ?>"><?= $item['NAME']; ?>
                    <?= $item['IBLOCK_ELEMENT_IBLOCK_SECTION_NAME'] ? ' – ' . $item['IBLOCK_ELEMENT_IBLOCK_SECTION_NAME'] : ''; ?></li>
            <? } ?>
        </ul>
    </div>
<? } else { ?>
    <? ShowMessage(['TYPE' => 'ERROR', 'MESSAGE' => 'Элементы не найдены']); ?>
<? } ?>