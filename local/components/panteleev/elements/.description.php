<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

Loc::loadMessages(__FILE__);

$arComponentDescription = [
    'NAME' => Loc::getMessage('PANTELEEV_ELEMENT_COMPONENT_NAME'),
    'DESCRIPTION' => Loc::getMessage('PANTELEEV_ELEMENT_COMPONENT_DESCRIPTION'),
    'ICON' => '/images/icon.gif',
    'PATH' => [
        'ID' => 'content',
        'CHILD' => [
            'ID' => 'panteleev',
            'NAME' => Loc::getMessage('PANTELEEV_ELEMENT_COMPONENT_GROUP_NAME')
        ]
    ],
];