<?php

namespace Panteleev\Components;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;


// todo: эрмитаж
class Elements extends \CBitrixComponent
{
    private $name;

    private $parameters;

    private $errors = [];

    public function executeComponent()
    {
        $this->prepareComponent();
        $this->getParameters();

        $this->name = $this->request->get($this->parameters['QUERY_PARAMETER']);

        $this->arResult['ELEMENTS'] = $this->getElements();
        $this->arResult['ERRORS'] = $this->getErrors();

        $this->includeComponentTemplate();
    }

    private function prepareComponent()
    {
        try {
            Loader::includeModule('iblock');
        } catch (LoaderException $e) {
        }
    }

    private function getParameters()
    {
        $defaultParameters = [
            'QUERY_PARAMETER' => 'name',
            '~QUERY_PARAMETER' => 'name',
            'ELEMENT_COUNT' => '10',
            '~ELEMENT_COUNT' => '10',
            'CACHE_TIME' => '3600',
            '~CACHE_TIME' => '3600',
        ];

        $this->parameters = array_merge($defaultParameters, $this->arParams);

        if (!isset($this->parameters['IBLOCK_ID'])) {
            $this->errors[] = 'No IBLOCK_ID defined';
        }
    }

    private function getElements()
    {
        $filter = [
            'IBLOCK_ID' => $this->parameters['IBLOCK_ID'],
            'ACTIVE' => 'Y'
        ];

        if (!empty($this->name)) {
            $filter['NAME'] = '%' . htmlspecialcharsEx($this->name) . '%';
        }

        $query = ElementTable::getList([
            'filter' => $filter,
            'select' => ['IBLOCK_ID', 'ID', 'NAME', 'IBLOCK_SECTION_ID', 'IBLOCK_SECTION.NAME'],
            'limit' => $this->parameters['ELEMENT_COUNT'],
            'cache' => [
                'ttl' => 60,
                'cache_joins' => true,
            ]
        ]);

        $elements = [];
        while ($ar = $query->fetch()) {
            $arButtons = \CIBlock::GetPanelButtons(
                $this->parameters['IBLOCK_ID'],
                $ar['ID'],
                0,
                ['SECTION_BUTTONS' => false, 'SESSID' => false]
            );

            $ar['EDIT_LINK'] = $arButtons['edit']['edit_element']['ACTION_URL'];
            $ar['DELETE_LINK'] = $arButtons['edit']['delete_element']['ACTION_URL'];

            $elements[] = $ar;
        }

        return $elements;
    }

    private function getErrors()
    {
        return $this->errors;
    }
}