<?php

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

try {
    Loader::includeModule('iblock');
} catch (LoaderException $e) {
}

Loc::loadMessages(__FILE__);

$iblockTypes = \CIBlockParameters::GetIBlockTypes(['-' => ' ']);

$iblocks = [];
$res = \CIBlock::GetList(['SORT' => 'ASC'], [
    'TYPE' => $arCurrentValues['IBLOCK_TYPE_ID'] != '-' ? $arCurrentValues['IBLOCK_TYPE_ID'] : ''
]);
while ($ar = $res->Fetch()) {
    $iblocks[$ar['ID']] = '[' . $ar['ID'] . '] ' . $ar['NAME'];
}

$arComponentParameters = [
    'GROUPS' => [
        'ENTITY' => [
            'NAME' => Loc::getMessage('PANTELEEV_ELEMENT_SETTINGS_IBLOCK')
        ],
        'GENERAL' => [
            'NAME' => Loc::getMessage('PANTELEEV_ELEMENT_SETTINGS_GENERAL')
        ],
    ],
    'PARAMETERS' => [
        'IBLOCK_TYPE_ID' => [
            'PARENT' => 'ENTITY',
            'NAME' => Loc::getMessage('PANTELEEV_ELEMENT_SETTINGS_IBLOCK_TYPE'),
            'TYPE' => 'LIST',
            'VALUES' => $iblockTypes,
            'REFRESH' => 'Y'
        ],
        'IBLOCK_ID' => [
            'PARENT' => 'ENTITY',
            'NAME' => Loc::getMessage('PANTELEEV_ELEMENT_SETTINGS_IBLOCK'),
            'TYPE' => 'LIST',
            'VALUES' => $iblocks,
            'REFRESH' => 'Y',
        ],

        'QUERY_PARAMETER' => [
            'PARENT' => 'GENERAL',
            'NAME' => Loc::getMessage('PANTELEEV_ELEMENT_SETTINGS_GET_PARAMETER'),
            'TYPE' => 'STRING',
            'DEFAULT' => 'name',
        ],
        'ELEMENT_COUNT' => [
            'PARENT' => 'GENERAL',
            'NAME' => Loc::getMessage('PANTELEEV_ELEMENT_SETTINGS_LIMIT'),
            'TYPE' => 'STRING',
            'DEFAULT' => '10'
        ],
        'CACHE_TIME' => [],
    ]
];