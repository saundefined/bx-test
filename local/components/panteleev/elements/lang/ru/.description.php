<?php

$MESS['PANTELEEV_ELEMENT_COMPONENT_NAME'] = 'Элементы';
$MESS['PANTELEEV_ELEMENT_COMPONENT_DESCRIPTION'] = 'Компонент для Битрикса, который берет из ссылки GET-параметр "name" и выводит на странице 10 элементов инфоблока, в названии которых содержится значение "name" из адресной строки. Выборка должна кешироваться.';
$MESS['PANTELEEV_ELEMENT_COMPONENT_GROUP_NAME'] = 'Тестовые компоненты';
