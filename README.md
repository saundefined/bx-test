# Примеры компонентов

## Список элменетов
```php
<?$APPLICATION->IncludeComponent(
	"panteleev:elements", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE_ID" => "content",
		"IBLOCK_ID" => "1",
		"QUERY_PARAMETER" => "name",
		"ELEMENT_COUNT" => "10"
	),
	false
);?>
```

## Список пользователей
```php
<?$APPLICATION->IncludeComponent(
	"panteleev:users", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PAGE_SIZE" => "5"
	),
	false
);?>
```